package com.workbee.basics;

import com.google.common.collect.ImmutableMap;

public class Type
{
	public static final int INT = 0;
	public static final int BOOLEAN = 1;
	public static final int LONG = 2;
	public static final int STRING = 3;
	public static final int EMAIL  = 4;
	public static final int PHONE = 5;
	public static final int COUNTRY = 6;
	public static final int REQUEST_FORMAT = 7;
	public static final int PASSWORD = 8;

	public enum Dummy
	{
		RAJ("Raj", 20);

		public String name;

		public int age;

		Dummy(String name, int age)
		{
			this.name= name;
			this.age = age;
		}
	}

	public static final ImmutableMap<Integer, StringParser> TypeConversion= new ImmutableMap.Builder<Integer, StringParser>()
		.put(INT, value -> (value))
		.put(BOOLEAN, value -> Boolean.parseBoolean(value))
		.put(LONG, value -> Long.parseLong(value))
		.put(STRING, value -> String.valueOf(value))
		.put(EMAIL, value -> String.valueOf(value))
		.put(PHONE, value -> Long.parseLong(value))
		.put(STRING, value -> String.valueOf(value)).build();

	public static @FunctionalInterface interface StringParser<R>
	{
		Object apply(String s);
	}



}





